package edu.ucsd.cse110.library;

public class DVD extends Publication{
	@SuppressWarnings("unused")
	private String title;

	public DVD(String string, LateFeesStrategy lateFees) {
		super(lateFees);
		title=string;
	}
	
	@Override
	public boolean checkRentable(Publication p) {
		// TODO Auto-generated method stub
		return p.isCheckout();
	}
	

}
