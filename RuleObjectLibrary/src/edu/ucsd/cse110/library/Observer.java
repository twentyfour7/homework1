package edu.ucsd.cse110.library;

public interface Observer {
	public void update(String name, MemberType memberType, double fee);
}
