package edu.ucsd.cse110.library;

public class Book extends Publication {
	private String title;

	public Book(String string, LateFeesStrategy lateFees) {
		super(lateFees);
		title=string;
	}

	@Override
	public boolean checkRentable(Publication p) {
		// TODO Auto-generated method stub
		return p.isCheckout();
	}
	
	public String getTitle(){
		return title;
	}

}
