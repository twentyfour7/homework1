package edu.ucsd.cse110.library;

import java.time.LocalDate;

public interface ILibrary {
	public void checkoutPublication(LocalDate checkoutDate);
	public void returnPublication(LocalDate returnDate);
	public double getFee();
	public boolean hasFee();
}
