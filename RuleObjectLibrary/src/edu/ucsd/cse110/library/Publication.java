package edu.ucsd.cse110.library;

import java.time.LocalDate;

public abstract class Publication implements Rentable {

	private Member hasBook;
	private LocalDate checkoutDate;
	private LateFeesStrategy lateFees;
	
	public Publication(LateFeesStrategy lateFees) {
		this.lateFees = lateFees;
	}
	
	public void pubReturn(LocalDate returnDate) {
		lateFees.applyRule(this, returnDate);
		hasBook=null;
		checkoutDate=null;
	}
	
	public void checkout(Member member, LocalDate checkoutDate) {
		hasBook=member;
		this.checkoutDate = checkoutDate;
	}

	public boolean isCheckout() {
		return checkoutDate!=null;
	}

	public Member getMember() {
		return hasBook;
	}

	public LocalDate getCheckoutDate() {
		return checkoutDate;
	}
	
}
