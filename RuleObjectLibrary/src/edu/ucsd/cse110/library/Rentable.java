package edu.ucsd.cse110.library;

public interface Rentable {
	public boolean checkRentable(Publication pub);
	
}
