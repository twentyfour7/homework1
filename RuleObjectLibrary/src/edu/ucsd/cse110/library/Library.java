package edu.ucsd.cse110.library;

import edu.ucsd.cse110.library.rules.RuleObject;
import java.time.LocalDate;

/* This is a Facade class */
public class Library implements ILibrary{
	private Publication pub;
	private RuleObject rule;
	private Member member;
	
	public void checkoutPublication(LocalDate checkoutDate){
		pub.checkout(member, checkoutDate);
	}
	
	public void returnPublication(LocalDate returnDate){
		pub.pubReturn(returnDate);
		rule.LateReturn(member, pub, returnDate);
	}
	
	public double getFee(){
		return member.getDueFees();
	}
	
	public boolean hasFee(){
		return (member.getDueFees() > 0) ;
	}
	

}
