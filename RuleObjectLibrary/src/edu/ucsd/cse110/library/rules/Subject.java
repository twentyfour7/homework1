package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.Observer;

public interface Subject {
	public void registerObserver(Observer o);
	public void removeObserver(Observer o);
	public void notifyObserver(Observer o);
	
}
