package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.Member;

public class MaxStudentCharge extends CheckOutRule{
	public double MaxFee = 20;
	
	public double MaxCharge(Member name){
		if(name.getType().equals("Student")){
			return this.MaxFee;
		}
		else
			return 0;
	}
}
