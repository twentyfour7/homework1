package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.MemberType;


public class TypeAndDaysLateAssessor implements Assessor {
	private MemberType type;
	private int maxDays;
	
	@Override
	public boolean evaluate(Properties prop) {
		return prop.getType() == type
				&& prop.getDays()>maxDays;
	}

	public TypeAndDaysLateAssessor(MemberType type, int maxDays) {
		this.type = type;
		this.maxDays = maxDays;
	}

	@Override
	public String getErrors() {
		return null;
	}

}
