package edu.ucsd.cse110.library.rules;

public class ChargePerDayFeeAction implements Action {
	private double perDayFee;
	private int freeDays;
	
	public ChargePerDayFeeAction(double perDayFee, int freeDays) {
		this.perDayFee = perDayFee;
		this.freeDays = freeDays;
	}

	@Override
	public void execute(Properties prop) {
		double assessed = (prop.getDays()-freeDays)*perDayFee;
		prop.getMember().applyLateFee(assessed);
	}

	@Override
	public String getErrors() {
		return null;
	}

}
