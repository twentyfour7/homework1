package edu.ucsd.cse110.library.rules;

import java.time.LocalDate;
import java.util.ArrayList;

import edu.ucsd.cse110.library.Member;
import edu.ucsd.cse110.library.Observer;
import edu.ucsd.cse110.library.Publication;

public class RuleObject implements IRuleObject, Subject {
	private Assessor assessor;
	private ArrayList<Observer> observers;
	private Action action;
	private Result result;
	
	public RuleObject(Assessor assessor, Action action) {
		this.assessor = assessor;
		this.action = action;
		this.result = new Result();
	}
	
	public void LateReturn(Member member, Publication pub, LocalDate checkoutDate){
		if(checkoutDate.isAfter(pub.getCheckoutDate())){
			member.applyLateFee(2);
			notifyObserver((Observer)member);
		}
	}
	
	@Override
	public boolean checkRule(Properties prop) {
		if(assessor.evaluate(prop)) {
			action.execute(prop);
			result.setActionResults(action.getErrors());
			result.setAssessorResults(assessor.getErrors());
			return true;
		}
		result.setAssessorResults(assessor.getErrors());
		return false;
	}

	@Override
	public Result getResults() {
		return result;
	}

	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeObserver(Observer o) {
		// TODO Auto-generated method stub
		int i = observers.indexOf(o);
		if(i >= 0){
			observers.remove(o);
		}
	}

	@Override
	public void notifyObserver(Observer o) {
		// TODO Auto-generated method stub
		o.notify();
	}

}
