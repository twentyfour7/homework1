package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.Member;

public abstract class CheckOutRule {
	private Member name;
	private int number = 5;
	
	public int CheckOutDay(){
		return 3;
	}
	
	public boolean renewals(){
		if(name.getType().equals("Students")){
			return true;
		}
		else if(name.getType().equals("Teacher")){
			return true;
		}
		else
		return false;
	}
	
	public int MaxCheckoutNumber(){
		return this.number;
	}
	
}
