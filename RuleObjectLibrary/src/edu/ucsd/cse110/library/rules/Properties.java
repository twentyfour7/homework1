package edu.ucsd.cse110.library.rules;

import java.time.LocalDate;
import java.time.Period;

import edu.ucsd.cse110.library.Member;
import edu.ucsd.cse110.library.MemberType;
import edu.ucsd.cse110.library.Publication;

public class Properties {
	int days;
	LocalDate checkoutDate;
	Member member;
	
	public int getDays() {
		return days;
	}
	public LocalDate getCheckoutDate() {
		return checkoutDate;
	}
	public MemberType getType() {
		return member.getType();
	}
	public double getCurrentFee() {
		return member.getDueFees();
	}
	public Member getMember() {
		return member;
	}
	
	public Properties(Publication pub, LocalDate returnDate) {
		this.member = pub.getMember();
		this.checkoutDate = pub.getCheckoutDate();
		this.days = Period.between(checkoutDate, returnDate).getDays();
	}
	
	
}
