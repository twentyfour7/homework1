package edu.ucsd.cse110.library.rules;

public interface IRuleObject {
	public boolean checkRule(Properties prop);
	public Result getResults();
}
