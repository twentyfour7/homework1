package edu.ucsd.cse110.library;

import java.time.LocalDate;


public interface LateFeesStrategy {
	public void applyRule(Publication publication, LocalDate returnDate);
}
