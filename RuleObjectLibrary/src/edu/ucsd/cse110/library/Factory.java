package edu.ucsd.cse110.library;

import edu.ucsd.cse110.library.rules.RuleObject;

public class Factory extends Library {
	private RuleObject rule;
	
	public Factory(RuleObject rule){
		this.rule = rule;
	}
	
	public RuleObject createRules(){
		return rule;
	}
		
}
