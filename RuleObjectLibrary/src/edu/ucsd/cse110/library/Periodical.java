package edu.ucsd.cse110.library;

public class Periodical extends Publication {
	private String title;

	public Periodical(String string, LateFeesStrategy lateFees) {
		super(lateFees);
		title=string;
	}
	
	@Override
	public boolean checkRentable(Publication p) {
		// TODO Auto-generated method stub
		return p.isCheckout();
	}
	
	public String getTitle(){
		return title;
	}

}
